<x-app-layout>
    <x-slot name="header">
        Üye Oluştur

    </x-slot>  
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">        
                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                @endif

                @if (session('success'))
                    <div class="alert alert-success">
                        {{session('success')}}
                    </div>
                @endif
                <br>
                <form action="{{route('users.store')}}" method="post">
                    @csrf
                    <div class="form-group">
                        <label>Üye Adı</label>
                        <input type="text" name="name" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Eposta</label>
                        <input type="text" name="email" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Şifre</label>
                        <input type="password" name="password" class="form-control" required>
                    </div>
                    <br><br>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-sm btn-block">Yeni Üye Oluştur</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>