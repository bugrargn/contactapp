<x-app-layout>
    <x-slot name="header">
        Üye Düzenle

    </x-slot>  
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">        
                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                @endif

                @if (session('success'))
                    <div class="alert alert-success">
                        {{session('success')}}
                    </div>
                @endif
                <br>
                <form action="{{route('users.update', $kullanicilar->id)}}" method="post">
                    @method('PUT')
                    @csrf
                    <div class="form-group">
                        <label>Üye Adı</label>
                        <input type="text" name="name" class="form-control" value="{{$kullanicilar->name}}" required>
                    </div>
                    <div class="form-group">
                        <label>Eposta</label>
                        <input type="text" name="email" value="{{$kullanicilar->email}}" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Şifre</label>
                        <input type="password" name="password" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Tür</label>
                        <select name="type" id="">
                            <option value="user">User</option>
                            <option value="admin">Admin</option>
                        </select>
                    </div>
                    <br><br>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-sm btn-block">Üyeyi Düzenle</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>