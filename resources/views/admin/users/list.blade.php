<x-app-layout>
    <x-slot name="header">
        Kullanıcılar
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <a href="{{route('users.create')}}" class="btn btn-lg btn-primary">Üye Oluştur</a>
                <form action="" method="get">
                    <div class="form-row">
                        <div class="col-md-4">
                            <input type="text" name="search" placeholder="Üye Adı" class="form-control" id="">
                        </div>
                    </div>
                </form>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">İsim</th>
                            <th scope="col">Eposta</th>
                            <th scope="col">Tür</th>
                            <th scope="col">İşlem</th>
                        </tr>
                    </thead>
                    @foreach ($kullanicilar as $kull)
                        <tr>
                            <th> {{$kull->id}} </th>
                            <td> {{$kull->name}} </td>
                            <td> {{$kull->email}} </td>
                            <td> {{$kull->type}} </td>
                            <td>
                                <a href="{{route('users.edit', $kull->id)}}" class="btn btn-sm btn-primary">Düzenle</a>
                                <a href="{{route('users.destroy', $kull->id)}}" class="btn btn-sm btn-danger">Sil</a>
                            </td>
                        </tr>                        
                    @endforeach                    
                </table>
                {{$kullanicilar->links()}}
            </div>
        </div>
    </div>
</x-app-layout>
