<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
        $kullanicilar = User::paginate(5);
        if(isset($_GET['search'])) {
            $searchText = $_GET['search'];
            $kullanicilar = User::where('name', 'LIKE', '%'.$searchText.'%')->paginate(5);
        }
        return view('admin.users.list', compact('kullanicilar'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        User::create($request->post());
        return redirect()->route('users.index')->withSuccess('Üye başarıyla oluşturuldu.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $kullanicilar = User::find($id) ?? abort(404, 'Üye bulunamadı');
        return view('admin.users.edit', compact('kullanicilar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $kullanicilar = User::find($id) ?? abort(404, 'Üye bulunamadı');
        User::where('id', $id)->update($request->except(['_method', '_token']));
        return redirect()->route('users.index')->withSuccess('Üye güncelleme başarıyla gerçekleşti');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $kullanicilar = User::find($id) ?? abort(404, 'Üye bulunamadı');
        $kullanicilar->delete();
        return redirect()->route('users.index')->withSuccess('Üye silme işlemi başarıyla gerçekleşti');
    }
}
